import React, { useState, useEffect } from 'react';
import './UserForm.css';

const UserForm = () => {
  const initialFormData = {
    firstName: '',
    lastName: '',
    age: '',
    employed: false,
    favoriteColor: '',
    sauces: {
      ketchup: false,
      mustard: false,
      mayonnaise: false,
      guacamole: false,
    },
    bestStooge: 'Larry',
    notes: '',
  };

  const [formData, setFormData] = useState(initialFormData);
  const [formDirty, setFormDirty] = useState(false);
  const [formValid, setFormValid] = useState(false);

  useEffect(() => {
    validateForm();
  }, [formData]);

  const handleInputChange = (e) => {
    const { name, value, type, checked } = e.target;

    setFormDirty(true);

    if (type === 'checkbox') {
      setFormData({
        ...formData,
        sauces: {
          ...formData.sauces,
          [name]: checked,
        },
      });
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };

  const validateForm = () => {
    const isFirstNameValid = /^[A-Za-z\s]+$/.test(formData.firstName);
    const isLastNameValid = /^[A-Za-z\s]+$/.test(formData.lastName);
    const isAgeValid = /^\d+$/.test(formData.age);
    const isNotesValid = formData.notes.length <= 100;

    const isFormValid =
      isFirstNameValid &&
      isLastNameValid &&
      isAgeValid &&
      isNotesValid;

    setFormValid(isFormValid);
  };

  const handleReset = () => {
    setFormData(initialFormData);
    setFormDirty(false);
    setFormValid(false);
  };

  const handleSubmit = () => {
    if (formValid) {
      alert(JSON.stringify(formData, null, 2));
    }
  };

  return (
    <div className="user-form">
      <div className="form-group">
        <label>
          First Name:
          <input
            type="text"
            name="firstName"
            value={formData.firstName}
            onChange={handleInputChange}
            className={!formValid && formDirty && !/^[A-Za-z\s]+$/.test(formData.firstName) ? 'invalid' : ''}
          />
        </label>
      </div>

      <div className="form-group">
        <label>
          Last Name:
          <input
            type="text"
            name="lastName"
            value={formData.lastName}
            onChange={handleInputChange}
            className={!formValid && formDirty && !/^[A-Za-z\s]+$/.test(formData.lastName) ? 'invalid' : ''}
          />
        </label>
      </div>

      <div className="form-group">
        <label>
          Age:
          <input
            type="text"
            name="age"
            value={formData.age}
            onChange={handleInputChange}
            className={!formValid && formDirty && !/^\d+$/.test(formData.age) ? 'invalid' : ''}
          />
        </label>
      </div>

      <div className="form-group">
        <label>
          Employed:
          <input
            type="checkbox"
            name="employed"
            checked={formData.employed}
            onChange={handleInputChange}
          />
        </label>
      </div>

      <div className="form-group">
        <label>
          Favorite Color:
          <select
            name="favoriteColor"
            value={formData.favoriteColor}
            onChange={handleInputChange}
          >
            <option value="">Select Color</option>
            <option value="Red">Red</option>
            <option value="Blue">Blue</option>
            <option value="Green">Green</option>
          </select>
        </label>
      </div>

      <div className="form-group">
        <label>
          Sauces:
          <div>
            <label>
              Ketchup
              <input
                type="checkbox"
                name="ketchup"
                checked={formData.sauces.ketchup}
                onChange={handleInputChange}
              />
            </label>
            <label>
              Mustard
              <input
                type="checkbox"
                name="mustard"
                checked={formData.sauces.mustard}
                onChange={handleInputChange}
              />
            </label>
            <label>
              Mayonnaise
              <input
                type="checkbox"
                name="mayonnaise"
                checked={formData.sauces.mayonnaise}
                onChange={handleInputChange}
              />
            </label>
            <label>
              Guacamole
              <input
                type="checkbox"
                name="guacamole"
                checked={formData.sauces.guacamole}
                onChange={handleInputChange}
              />
            </label>
          </div>
        </label>
      </div>

      <div className="form-group">
        <label>
          Best Stooge:
          <div>
            <label>
              Larry
              <input
                type="radio"
                name="bestStooge"
                value="Larry"
                checked={formData.bestStooge === 'Larry'}
                onChange={handleInputChange}
              />
            </label>
            <label>
              Moe
              <input
                type="radio"
                name="bestStooge"
                value="Moe"
                checked={formData.bestStooge === 'Moe'}
                onChange={handleInputChange}
              />
            </label>
            <label>
              Curly
              <input
                type="radio"
                name="bestStooge"
                value="Curly"
                checked={formData.bestStooge === 'Curly'}
                onChange={handleInputChange}
              />
            </label>
          </div>
        </label>
      </div>

      <div className="form-group">
        <label>
          Notes:
          <textarea
            name="notes"
            value={formData.notes}
            onChange={handleInputChange}
            className={!formValid && formDirty && formData.notes.length > 100 ? 'invalid' : ''}
          />
        </label>
      </div>

      <div className="buttons">
        <button type="reset" onClick={handleReset} disabled={!formDirty}>
          Reset
        </button>
        <button type="button" onClick={handleSubmit} disabled={!formValid}>
          Submit
        </button>
      </div>
    </div>
  );
};

export default UserForm;
